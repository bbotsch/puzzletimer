const std = @import("std");
const puzzle = @import("puzzle");
const js = @import("js.zig");

const ScrambleGenerator = @This();

id: []const u8,
elem: js.Element,
data: struct {
    scramble: []const u8,
    counter: u8,
},

pub const template = @embedFile("templates/scramble_generator.html");

pub fn init(id: []const u8, prng: std.rand.Random, allocator: std.mem.Allocator) !ScrambleGenerator {
    const parent = try js.document.getElementById("ScrambleGenerator");
    defer parent.deinit();

    const elem = try js.document.createElement("div");
    try elem.setId(id);
    _ = try parent.appendChild(elem);

    const result: ScrambleGenerator = .{
        .id = id,
        .elem = elem,
        .data = .{
            .scramble = try puzzle.generateScrambleString(prng, allocator),
            .counter = 0,
        },
    };
    try result.createElement(allocator);
    return result;
}

pub fn renderHtml(self: ScrambleGenerator, writer: std.io.AnyWriter) !void {
    try writer.print(template, self.data);
}

pub fn createElement(self: ScrambleGenerator, allocator: std.mem.Allocator) !void {
    var html = try std.ArrayListUnmanaged(u8).initCapacity(allocator, 60);
    defer html.deinit(allocator);
    try self.renderHtml(html.writer(allocator).any());
    try self.elem.setInnerHtml(html.items);
}

pub fn nextScramble(self: *ScrambleGenerator, prng: std.rand.Random, allocator: std.mem.Allocator) !void {
    allocator.free(self.data.scramble);
    self.data.scramble = try puzzle.generateScrambleString(prng, allocator);
    self.data.counter += 1;

    var html = try std.ArrayListUnmanaged(u8).initCapacity(allocator, 40);
    defer html.deinit(allocator);
    try self.renderHtml(html.writer(allocator).any());

    try self.elem.setInnerHtml(html.items);
}
