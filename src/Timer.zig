const std = @import("std");
const js = @import("js.zig");
const web = @import("puzzletimer-web.zig");

const Timer = @This();

id: []const u8,
start_value: f32,
state: enum { stopped, waiting, ready, started },
elem: js.Element,
waiting_timeout: ?u32,
update_interval: ?u32,
data: struct {
    state: []const u8,
    value: f32,
},

pub const State = struct {};

pub const template = @embedFile("templates/timer.html");

pub fn init(id: []const u8, allocator: std.mem.Allocator) !Timer {
    const parent = try js.document.getElementById("Timer");
    defer parent.deinit();

    const elem = try js.document.createElement("div");
    try elem.setId(id);
    _ = try parent.appendChild(elem);

    const result: Timer = .{
        .id = id,
        .start_value = 0,
        .state = .stopped,
        .elem = elem,
        .waiting_timeout = null,
        .update_interval = null,
        .data = .{
            .state = "stopped",
            .value = 0,
        },
    };
    try result.updateHtml(allocator);
    return result;
}

pub fn renderHtml(self: Timer, writer: std.io.AnyWriter) !void {
    try writer.print(template, self.data);
}

pub fn updateHtml(self: Timer, allocator: std.mem.Allocator) !void {
    var html = try std.ArrayListUnmanaged(u8).initCapacity(allocator, 40);
    defer html.deinit(allocator);
    try self.renderHtml(html.writer(allocator).any());
    try self.elem.setInnerHtml(html.items);
}

pub fn handleKeyDownEvent(self: *Timer, key: u8, allocator: std.mem.Allocator) !void {
    switch (self.state) {
        .waiting, .ready => return,
        .stopped => {
            if (key == ' ') {
                self.state = .waiting;
                self.data.state = "waiting";
                self.waiting_timeout = try js.setTimeout("resetTimer", 1000);
            } else return;
        },
        .started => {
            const now = try js.performance.now();
            const final_time = (now - self.start_value) / 1000;
            try js.clearInterval(self.update_interval orelse return error.IntervalNotActive);
            self.update_interval = null;
            self.data.value = final_time;
            try web.unsetBodyStarted();
            web.nextScramble();
            self.state = .stopped;
            self.data.state = "stopped";
        },
    }
    try self.updateHtml(allocator);
}

pub fn handleKeyUpEvent(self: *Timer, key: u8, allocator: std.mem.Allocator) !void {
    if (key != ' ') return;
    switch (self.state) {
        .stopped, .started => return,
        .waiting => {
            self.state = .stopped;
            self.data.state = "stopped";
            try js.clearTimeout(self.waiting_timeout orelse return error.TimeoutNotActive);
            self.waiting_timeout = null;
        },
        .ready => {
            self.start_value = try js.performance.now();
            self.state = .started;
            self.data.state = "started";
            self.update_interval = try js.setInterval("updateTimer", 37);
        },
    }
    try self.updateHtml(allocator);
}

pub fn reset(self: *Timer, allocator: std.mem.Allocator) !void {
    self.data.value = 0;
    self.state = .ready;
    self.data.state = "ready";
    try self.updateHtml(allocator);
    try web.setBodyStarted();
}

pub fn update(self: *Timer, allocator: std.mem.Allocator) !void {
    const now = try js.performance.now();
    self.data.value = (now - self.start_value) / 1000;
    try self.updateHtml(allocator);
}
