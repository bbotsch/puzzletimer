// raylib-zig (c) Nikolas Wipper 2023

const std = @import("std");
const builtin = @import("builtin");
const rl = @import("raylib");
const puzzle = @import("puzzle");

const State = enum {
    stopped,
    waiting,
    ready,
    started,
};

const use_c_allocator = ((builtin.mode != .Debug) or (builtin.os.tag == .wasi)); // .wasi instead of .emscripten because of hack in raylib-zig

pub fn main() !void {
    var gpa = if (!use_c_allocator) std.heap.GeneralPurposeAllocator(.{}){} else void{};
    defer if (!use_c_allocator) std.debug.assert(gpa.deinit() == .ok);
    const allocator = if (use_c_allocator) std.heap.c_allocator else gpa.allocator();

    const seed: u64 = if (builtin.os.tag == .wasi) @intCast(std.time.nanoTimestamp()) else blk: {
        var seed: u64 = undefined;
        try std.posix.getrandom(std.mem.asBytes(&seed));
        break :blk seed;
    };
    var _prng = std.rand.DefaultPrng.init(seed);
    const prng = _prng.random();

    try puzzle.init(allocator);
    defer puzzle.deinit(allocator);

    // Initialization
    //--------------------------------------------------------------------------------------
    const screenWidth = 800;
    const screenHeight = 450;

    rl.initWindow(screenWidth, screenHeight, "raylib-zig [core] example - basic window");
    defer rl.closeWindow(); // Close window and OpenGL context

    const framerate = @max(rl.getMonitorRefreshRate(0), 60);
    rl.setTargetFPS(framerate); // Set our game to run at 60 frames-per-second
    //--------------------------------------------------------------------------------------

    const font = rl.Font.init("/usr/share/fonts/iosevka-nerdfont/IosevkaNerdFontMono-ExtraBold.ttf");
    defer font.unload();

    const stopped_color = rl.Color.fromInt(0x00FFFFFF);
    const waiting_color = rl.Color.fromInt(0xDC143CFF);
    const ready_color = rl.Color.fromInt(0x00FA9AFF);
    const started_color = rl.Color.fromInt(0xA9A9A9FF);
    const scramble_color = rl.Color.fromInt(0xFAF0E6FF);

    const max_line_length = 55;
    rl.setTextLineSpacing(35);

    var scramble = try newScramble(max_line_length, prng, allocator);
    defer allocator.free(scramble);

    var timer = try std.time.Timer.start();
    var timer_value: u64 = 0;
    var time_buffer: [32]u8 = undefined;

    var state: State = .stopped;
    var space_timer = try std.time.Timer.start();

    const no_key: rl.KeyboardKey = @enumFromInt(0);

    // Main game loop
    while (!rl.windowShouldClose()) { // Detect window close button or ESC key
        // Update
        //----------------------------------------------------------------------------------
        // TODO: Update your variables here
        switch (state) {
            .stopped => {
                if (rl.isKeyDown(rl.KeyboardKey.key_space)) {
                    state = .waiting;
                    space_timer.reset();
                }
            },
            .waiting => {
                if (rl.isKeyUp(rl.KeyboardKey.key_space)) {
                    state = .stopped;
                } else if (space_timer.read() >= std.time.ns_per_s) {
                    state = .ready;
                    timer_value = 0;
                }
            },
            .ready => {
                if (rl.isKeyUp(rl.KeyboardKey.key_space)) {
                    state = .started;
                    timer.reset();
                }
            },
            .started => {
                timer_value = timer.read();
                if (rl.getKeyPressed() != no_key) {
                    state = .stopped;
                    const new_scramble = try newScramble(max_line_length, prng, allocator);
                    allocator.free(scramble);
                    scramble = new_scramble;
                }
            },
        }
        const seconds = timer_value / std.time.ns_per_s;
        const centis = (timer_value % std.time.ns_per_s) / (std.time.ns_per_ms * 10);
        const time_string = if (state == .started)
            try std.fmt.bufPrintZ(&time_buffer, "{: >2}.{}", .{ seconds, centis / 10 })
        else
            try std.fmt.bufPrintZ(&time_buffer, "{: >2}.{:0>2}", .{ seconds, centis });
        //----------------------------------------------------------------------------------

        // Draw
        //----------------------------------------------------------------------------------
        rl.beginDrawing();
        defer rl.endDrawing();

        switch (state) {
            .stopped => {
                rl.clearBackground(rl.Color.black);
                rl.drawTextEx(font, scramble, rl.Vector2.init(25, 20), 32, 1, scramble_color);
                rl.drawText(time_string, 300, 200, 100, stopped_color);
            },
            .waiting => {
                rl.clearBackground(rl.Color.black);
                rl.drawTextEx(font, scramble, rl.Vector2.init(25, 20), 32, 1, scramble_color);
                rl.drawText(time_string, 300, 200, 100, waiting_color);
            },
            .ready => {
                rl.clearBackground(rl.Color.white);
                rl.drawText(time_string, 300, 200, 100, ready_color);
            },
            .started => {
                rl.clearBackground(rl.Color.white);
                rl.drawText(time_string, 300, 200, 100, started_color);
            },
        }
        //----------------------------------------------------------------------------------
    }
}

fn formatScramble(scramble: []u8, max_line_length: usize, allocator: std.mem.Allocator) ![:0]const u8 {
    const scrambleZ = try allocator.dupeZ(u8, scramble);
    errdefer allocator.free(scrambleZ);
    var index: usize = 0;
    var count: usize = 0;
    while (index + max_line_length < scrambleZ.len) : (count += 1) {
        const len = std.mem.lastIndexOfAny(u8, scrambleZ[index..][0..max_line_length], &std.ascii.whitespace) orelse return error.CannotFormatScramble;
        const end = index + len;
        scrambleZ[end] = '\n';
        index = end + 1;
    }
    return scrambleZ;
}

fn newScramble(max_line_length: usize, prng: std.rand.Random, allocator: std.mem.Allocator) ![:0]const u8 {
    const scramble = try puzzle.generateScrambleString(prng, allocator);
    defer allocator.free(scramble);
    std.log.info("Generated scramble: {s}", .{scramble});
    return try formatScramble(scramble, max_line_length, allocator);
}
