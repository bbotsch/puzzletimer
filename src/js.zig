const std = @import("std");
const zjs = @import("zig-js");

pub const string = zjs.string;

pub fn init() !void {
    try console.init();
    try performance.init();
    try document.init();
    try exports.init();
}

pub const console = struct {
    pub var ref: zjs.Object = undefined;

    pub fn init() !void {
        ref = try zjs.global.get(zjs.Object, "console");
    }

    pub fn log(arg: anytype) !void {
        try ref.call(void, "log", .{zjs.Value.init(arg)});
    }
};

pub const performance = struct {
    pub var ref: zjs.Object = undefined;

    pub fn init() !void {
        ref = try zjs.global.get(zjs.Object, "performance");
    }

    pub fn now() !f32 {
        return try ref.call(f32, "now", .{});
    }
};

pub const document = struct {
    pub var ref: zjs.Object = undefined;
    pub var body: Element = undefined;

    pub fn init() !void {
        ref = try zjs.global.get(zjs.Object, "document");
        body = try Element.init(try ref.get(zjs.Object, "body"));
    }

    pub fn getElementById(id: []const u8) !Element {
        return try Element.init(try ref.call(zjs.Object, "getElementById", .{zjs.string(id)}));
    }

    pub fn createElement(tag: []const u8) !Element {
        return try Element.init(try ref.call(zjs.Object, "createElement", .{zjs.string(tag)}));
    }
};

pub const exports = struct {
    pub var ref: zjs.Object = undefined;

    pub fn init() !void {
        ref = try zjs.global.get(zjs.Object, "wasm");
    }

    pub fn get(comptime name: []const u8) !zjs.Object {
        const Func = struct {
            name: []const u8 = name, // Force the compiler to generate a new type for each unique name
            var ref: ?zjs.Object = null;
        };
        if (Func.ref == null) {
            Func.ref = try ref.get(zjs.Object, name);
        }
        return Func.ref.?;
    }
};

pub const Element = struct {
    ref: zjs.Object,
    classList: zjs.Object,

    pub fn init(obj: zjs.Object) !Element {
        return .{
            .ref = obj,
            .classList = try obj.get(zjs.Object, "classList"),
        };
    }

    pub fn deinit(self: Element) void {
        self.ref.deinit();
    }

    pub fn setId(self: Element, id: []const u8) !void {
        try self.ref.set("id", zjs.string(id));
    }

    pub fn setInnerHtml(self: Element, html: []const u8) !void {
        try self.ref.set("innerHTML", zjs.string(html));
    }

    pub fn setInnerText(self: Element, text: []const u8) !void {
        try self.ref.set("innerText", zjs.string(text));
    }

    pub fn appendChild(self: Element, child: Element) !Element {
        return try Element.init(try self.ref.call(zjs.Object, "appendChild", .{child.ref}));
    }
};

/// Returns a u32 representing a handle for the timeout that can be passed into clearTimeout
pub fn setTimeout(comptime fn_name: []const u8, delay: f32) !u32 {
    const f = try exports.get(fn_name);
    return try zjs.global.call(u32, "setTimeout", .{ f, delay });
}

pub fn clearTimeout(timeoutId: u32) !void {
    try zjs.global.call(void, "clearTimeout", .{timeoutId});
}

/// Returns a u32 representing a handle for the interval that can be passed into clearInterval
pub fn setInterval(comptime fn_name: []const u8, delay: f32) !u32 {
    const f = try exports.get(fn_name);
    return try zjs.global.call(u32, "setInterval", .{ f, delay });
}

pub fn clearInterval(intervalId: u32) !void {
    try zjs.global.call(void, "clearInterval", .{intervalId});
}
