const std = @import("std");
const builtin = @import("builtin");
const puzzle = @import("puzzle");
const js = @import("js.zig");
const ScrambleGenerator = @import("ScrambleGenerator.zig");
const Timer = @import("Timer.zig");

pub const std_options = .{
    .log_level = .info,
    .logFn = consoleLog,
};

var _prng: std.rand.DefaultPrng = undefined;
var prng: std.rand.Random = undefined;

var gpa: std.heap.GeneralPurposeAllocator(.{}) = .{};
const allocator = gpa.allocator();

pub fn consoleLog(comptime level: std.log.Level, comptime scope: @TypeOf(.EnumLiteral), comptime format: []const u8, args: anytype) void {
    _ = level; // autofix
    _ = scope; // autofix
    var s = std.ArrayListUnmanaged(u8).initCapacity(allocator, 20) catch return;
    defer s.deinit(allocator);
    s.writer(allocator).print(format, args) catch return;
    js.console.log(js.string(s.items)) catch unreachable;
}

fn ErrorUnionPayload(error_union: anytype) type {
    const tinfo = @typeInfo(error_union).ErrorUnion;
    return tinfo.payload;
}

inline fn tryFunc(ret: anytype) ErrorUnionPayload(@TypeOf(ret)) {
    return ret catch |err| {
        std.log.err("Error: {}", .{err});
        std.debug.panic("Error: {}", .{err});
    };
}

export fn init(seed: u64) void {
    tryFunc(puzzle.init(allocator));
    _prng = std.rand.DefaultPrng.init(seed);
    prng = _prng.random();

    tryFunc(js.init());

    scramble_generator = tryFunc(ScrambleGenerator.init("scramble-generator-0", prng, allocator));
    timer = tryFunc(Timer.init("timer-0", allocator));
}

var scramble_generator: ScrambleGenerator = undefined;

pub export fn nextScramble() void {
    tryFunc(scramble_generator.nextScramble(prng, allocator));
}

var timer: Timer = undefined;

export fn handleKeyDownEvent(key: u8) void {
    tryFunc(timer.handleKeyDownEvent(key, allocator));
}

export fn handleKeyUpEvent(key: u8) void {
    tryFunc(timer.handleKeyUpEvent(key, allocator));
}

export fn resetTimer() void {
    tryFunc(timer.reset(allocator));
}

export fn updateTimer() void {
    tryFunc(timer.update(allocator));
}

pub fn setBodyStarted() !void {
    try js.document.body.classList.call(void, "add", .{js.string("started")});
    const sg = try js.document.getElementById("ScrambleGenerator");
    try sg.classList.call(void, "add", .{js.string("invisible")});
}

pub fn unsetBodyStarted() !void {
    try js.document.body.classList.call(void, "remove", .{js.string("started")});
    const sg = try js.document.getElementById("ScrambleGenerator");
    try sg.classList.call(void, "remove", .{js.string("invisible")});
}
