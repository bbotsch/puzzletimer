const std = @import("std");
const rl = @import("raylib-zig");

pub fn build(b: *std.Build) !void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const puzzle = if (target.result.os.tag == .emscripten)
        // std.compress decompressors doesn't work in emscripten for some reason
        b.dependency("puzzlesolver", .{ .compression = .none }).module("puzzle")
    else
        b.dependency("puzzlesolver", .{}).module("puzzle");

    if (target.result.os.tag == .freestanding) {
        // Wasm for web
        const zig_js = b.dependency("zig-js", .{}).module("zig-js");
        const puzzletimer = b.addExecutable(.{
            .name = "puzzletimer",
            .root_source_file = .{ .path = "src/puzzletimer-web.zig" },
            .optimize = optimize,
            .target = target,
        });
        puzzletimer.root_module.addImport("puzzle", puzzle);
        puzzletimer.root_module.addImport("zig-js", zig_js);
        puzzletimer.entry = .disabled;
        puzzletimer.rdynamic = true;
        // puzzletimer.import_memory = true;
        puzzletimer.export_memory = true;
        // puzzletimer.stack_size = std.wasm.page_size * 2;
        // puzzletimer.initial_memory = std.wasm.page_size * 15;
        // puzzletimer.max_memory = std.wasm.page_size * 400;

        b.installArtifact(puzzletimer);

        const run_cmd = b.addSystemCommand(&.{ "python", "-m", "http.server", "-d", b.pathFromRoot("www") });
        run_cmd.step.dependOn(b.getInstallStep());
        const run_step = b.step("run", "Run puzzletimer");
        run_step.dependOn(&run_cmd.step);
        return;
    }

    const raylib_zig = b.dependency("raylib-zig", .{
        .target = target,
        .optimize = optimize,
    });
    const raylib = raylib_zig.module("raylib");
    const raylib_math = raylib_zig.module("raylib-math");

    if (target.result.os.tag == .emscripten) {
        if (b.sysroot == null) {
            const emsdk_dir = try std.process.getEnvVarOwned(b.allocator, "EMSDK");
            const sysroot = b.pathJoin(&.{ emsdk_dir, "upstream", "emscripten" });
            b.sysroot = sysroot;
        }

        const exe_lib = rl.compileForEmscripten(b, "puzzletimer", "src/main.zig", target, optimize);
        exe_lib.root_module.addImport("raylib", raylib);
        exe_lib.root_module.addImport("raylib-math", raylib_math);
        exe_lib.root_module.addImport("puzzle", puzzle);
        const raylib_artifact = rl.getRaylib(b, target, optimize);
        // Note that raylib itself is not actually added to the exe_lib output file, so it also needs to be linked with emscripten.
        exe_lib.linkLibrary(raylib_artifact);
        const link_step = try rl.linkWithEmscripten(b, &[_]*std.Build.Step.Compile{ exe_lib, raylib_artifact });
        b.getInstallStep().dependOn(&link_step.step);
        const run_step = try rl.emscriptenRunStep(b);
        run_step.step.dependOn(&link_step.step);
        const run_option = b.step("run", "Run puzzletimer");
        run_option.dependOn(&run_step.step);
        return;
    }

    const exe = b.addExecutable(.{
        .name = "puzzletimer",
        .root_source_file = .{ .path = "src/main.zig" },
        .optimize = optimize,
        .target = target,
    });

    rl.link(b, exe, target, optimize);
    exe.root_module.addImport("raylib", raylib);
    exe.root_module.addImport("raylib-math", raylib_math);
    exe.root_module.addImport("puzzle", puzzle);

    const run_cmd = b.addRunArtifact(exe);
    const run_step = b.step("run", "Run puzzletimer");
    run_step.dependOn(&run_cmd.step);

    b.installArtifact(exe);
}
