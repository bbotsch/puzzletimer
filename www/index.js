import { ZigJS} from "./zig-js-glue.js";

const zigjs = new ZigJS();

WebAssembly.instantiateStreaming(fetch("puzzletimer.wasm"), zigjs.importObject()).then((result) => {
    window.wasm = result.instance.exports;
    zigjs.memory = wasm.memory;

    const seed = BigInt(Date.now());
    wasm.init(seed);

    document.addEventListener("keydown", (event) => {
        wasm.handleKeyDownEvent(event.key.charCodeAt(0));
    });

    document.addEventListener("keyup", (event) => {
        wasm.handleKeyUpEvent(event.key.charCodeAt(0));
    });
});
